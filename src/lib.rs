#![doc = include_str!("../README.md")]
#![no_std]

/// Receiver type for disuse return value.
#[derive(Debug, Default)]
pub struct Disuse;

/// Dispose of the return value.
/// Type `T` should implement [Clone].
impl<T: Clone> From<T> for Disuse {
    fn from(_: T) -> Self { Self }
}