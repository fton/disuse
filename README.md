[![Latest Release](https://gitlab.com/fton/disuse/-/badges/release.svg)](https://gitlab.com/fton/disuse/-/releases)
[![pipeline status](https://gitlab.com/fton/disuse/badges/main/pipeline.svg)](https://gitlab.com/fton/disuse/-/commits/main)
[![coverage report](https://gitlab.com/fton/disuse/badges/main/coverage.svg)](https://gitlab.com/fton/disuse/-/commits/main)

# Description
The way to notify the implementation which return value is disuse.
# Usage
When an algorithm can calculate multiple useful values simultaneously, you will write like the following code.
```rust
pub fn some_algorithm(x: f64) -> (f64, f64, f64) {
    let mut r1 = 0f64; let mut r2 = 0f64; let mut r3 = 0f64;

    for _ in 0..1000 { // ... some heavy calculations here ...
        r1 += x;
        r2 += x * 2.0;
        r3 += x * 3.0;
    }
    (r1, r2, r3)
}

// But your users are not always need all return values.
// Yes, we can ignore the return values, but calculations done.

let (a, _, _) = some_algorithm(4.0);
```
Above example code can be rewritten using [`Disuse`][Disuse] as follows,
```rust
use disuse::Disuse;

pub fn some_algorithm<R1, R2, R3>(x: f64) -> (R1, R2, R3)
where
    R1: From<f64>, R2: From<f64>, R3: From<f64>
{
   let mut r1 = 0f64; let mut r2 = 0f64; let mut r3 = 0f64;

   for _ in 0..1000 { // ... heavy calculations here ...
       r1 += x;
       r2 += x * 2.0; // When user call this function like below,
       r3 += x * 3.0; // we can expect the compiler eliminate this two line, right?
   }
   (r1.into(), r2.into(), r3.into())
}

let (a, _, _): (f64, Disuse, Disuse) = some_algorithm(4.0);
```
It's better than before version.

(But return type should implement [`Clone`][clone] trait.)

If the unit type ([`()`][unit](unit)) implemented [`From`][from] trait like as follows,
```text
impl<T> From<T> for () {
    fn from(_: T) -> () { () }
}
```
above example code could be written more smart.
```text
pub fn some_algorithm<R1, R2, R3>(x: f64) -> (R1, R2, R3)
where
    R1: From<f64>, R2: From<f64>, R3: From<f64>
{
   let mut r1 = 0f64; let mut r2 = 0f64; let mut r3 = 0f64;

   for _ in 0..1000 { // ... heavy calculations here ...
       r1 += x;
       r2 += x * 2.0; // When user call this function like below,
       r3 += x * 3.0; // we can expect the compiler eliminate this two line, right?
   }
   (r1.into(), r2.into(), r3.into())
}

let (a, _, _): (f64, (), ()) = some_algorithm(4.0);
```
It's just unrealizable dream...

[clone]: https://doc.rust-lang.org/std/clone/trait.Clone.html
[unit]: https://doc.rust-lang.org/std/primitive.unit.html
[from]: https://doc.rust-lang.org/std/convert/trait.From.html
[Disuse]: https://docs.rs/disuse/*/disuse/struct.Disuse.html