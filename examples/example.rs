use disuse::Disuse;

//#[no_mangle]
#[inline(never)]
fn calc_r1(x: f64) -> f64 {
    println!("calculating r1...{}", x);
    x
}

//#[no_mangle]
#[inline(never)]
fn calc_r2(x: f64) -> f64 {
    println!("calculating r2...{}", x);
    x
}

//#[no_mangle]
#[inline(never)]
fn calc_r3(x: f64) -> f64 {
    println!("calculating r3...{}", x);
    x
}

#[inline(never)]
fn calc_rs<R1, R2, R3>(x: f64) -> (R1, R2, R3)
where
    R1: From<f64>, R2: From<f64>, R3: From<f64>,
{
    let mut r1 = 0f64;
    let mut r2 = 0f64;
    let mut r3 = 0f64;

    for _ in 1..5 {
        r1 += r1 * x + calc_r1(x);
        r2 += r2 * x * 2.0 + calc_r2(x) ;
        r3 *= r3 * x * 3.0 + calc_r3(x) ;
    }
    (r1.into(), r2.into(), r3.into())
}

fn main() {
    let (a, b, c): (f64, f64, f64) = calc_rs(2.1);

    println!("{}, {}, {}", a, b, c);

    let (a, _, _): (f64, Disuse, Disuse) = calc_rs(2.1);

    println!("{}", a);

    let (a, _, c): (f64, Disuse, f64) = calc_rs(2.1);

    println!("{}, {}", a, c);
}